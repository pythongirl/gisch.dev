+++
title = "Hexponent"
date = 2020-03-27
draft = true

[taxonomies]
tags = ["programming", "rust", "float"]
+++

I work on the [RCC][1] project, a C-compiler in written in rust, started by
Joshua Nelson. While fuzzing the project, we found that there were recurring
issues with [`hexf`][2] the library we used for hexadecimal float parsing.
`hexf` was generally unmaintained with no new commits in almost a year, so it
was unlikely that these bugs would be fixed.

I thought I might be able to write my own library to do the same thing, but the
thought of doing all the fancy math with floats was daunting.

Eventually I got up to the thought and started building a small example in the
[Rust Playground][3]. My first prototype only included parsing hexadecimal
floats to a struct representing the float.

```rust
struct FloatLiteral {
    sign: bool,
    ipart: Vec<u8>,
    fpart: Vec<u8>,
    exponent: i32,
}
```

This structure is a very literal representation of the text, but had some issues
that I will discuss later. The C11 standard definition for hexadecimal floating
point literals can be found [here][4]. I decided to be a little more lenient
than the specification because most programs using hexponent will already decide
what they give to hexponent in the first place. This will allow for other
projects to use a looser definition of hexedecimal float literals. The main
place where I decided to differ from the specification was om the exponent. The
specification requires an exponent, but that's not consistent with other float
literals, so I don't require it.

Parsing happens in a few steps, first the sign is parsed. Next the `0x` prefix
is parsed. Next zero or more hexadecimal digits are parsed, then a decimal
point, then zero or more hexidecimal digits. But there must be digits before or
after the decimal point, I do this by validating it right after parsing. Next an
exponent part may be present, composed of a letter `p`, then a signed decimal
number.

An odd bit of the spec is that the exponent part is represented in base-ten, and
raised to the power of two. This is a bit odd because at a first glance you
would assume that the exponent would be reperesented in base-16 and raised to
the power of 16.

The hard part of hexponent, was the converstion from `FloatLiteral` to floating-
point value. I achieved this by copying the digit's values into an unsigned
integer. After the digits have been copied into the integer, I check how many
leading zeros there are.

Explain floating point values first. (Mantissa, exponent, implicit bit)



One of my goals for this library was to parse byte slices instead of strings
because RCC works based on bytes, not UTF-8 encoded strings.

The parsing was relatively easy compared to the next srep, conversion.

While parsing was easy, there are lots of cases that
people don't think of when writing a float parser. For example, `0x1.5` is the
simplest case, but what about `0x1.` or `0x.5` or even no decimal at all like
`0x1`.

[1]: https://github.com/jyn514/rcc
[2]: https://github.com/lifthrasiir/hexf
[3]: https://play.rust-lang.org/
[4]: https://port70.net/~nsz/c/c11/n1570.html#6.4.4.2
