+++
title = "Polynomial Sum Method"
date = 2024-11-13
draft = true
author = "Julia Scheaffer"

[taxonomies]
tags = ["math"]
+++

Stuff goes here


## Faulhaber's Formula
<math display="block">
  <mrow>
    <munderover>
      <mo>∑</mo>
      <mrow><mi>k</mi><mo>=</mo><mn>1</mn></mrow>
      <mi>n</mi>
    </munderover>
    <msup><mi>k</mi><mi>p</mi></msup>
  </mrow>
  <mo>=</mo>
  <mrow>
    <mfrac>
      <mn>1</mn>
      <mrow><mi>p</mi><mo>+</mo><mn>1</mn></mrow>
    </mfrac>
    <munderover>
      <mo>∑</mo>
      <mrow><mi>r</mi><mo>=</mo><mn>0</mn></mrow>
      <mi>p</mi>
    </munderover>
    <mrow>
      <mo>(</mo>
      <mfrac linethickness="0">
        <mrow><mi>p</mi><mo>+</mo><mn>1</mn></mrow>
        <mi>r</mi>
      </mfrac>
      <mo>)</mo>
    </mrow>
    <msub><mi>B</mi><mi>r</mi></msub>
    <msup>
      <mi>n</mi>
      <mrow>
        <mi>p</mi><mo>-</mo><mi>r</mi><mo>+</mo><mn>1</mn>
      </mrow>
    </msup>
  </mrow>
</math>
