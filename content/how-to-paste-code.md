+++
title = "How to paste code into Discord"
date = 2020-09-02
+++
Hey, you probably tried to paste code into a discord channel, but unfortunatly,
it looked terrible. Thankfuly, Discord has a fancy feature where it won't mangle
your neatly formatted code! If you surround your code with three backticks (the
key right under escape on your keyboard) it won't mess up your code. Here's an
example:

If you type:
````
```
if 1 == 2:
    print("hello world")
```
````
Discord will print
> ```
> if 1 == 2:
>     print("hello world")
> ```
instead of
> if 1 == 2:
>     print("hello world")

You can also add the name of the programming language directly after the
backticks (and before the newline) to get fancy syntax highlighting. For
example, if you need help with your C assignment, you could type
````
```c
int main(){
    printf("Hello World\n");
}
```
````
to get
> ```c
> int main(){
>     printf("Hello World\n");
> }
> ```
This is much clearer and easier to read for those trying to help you. It works
with almost every programming language, not just C.